"""
    Lambda function to refund payment of a participant
"""
import json
import logging
from os import environ

import boto3
import requests
from botocore.exceptions import ClientError

from Utils.data_layer import DBConnection

DB_CONNECTION = DBConnection()
function_name = 'refund_money'

client = boto3.client('s3')
CONFERENCE_BUCKET_NAME = environ['CONFERENCE_BUCKET_NAME']
USER_BUCKET_NAME = environ['USER_BUCKET_NAME']

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def check_user_authenticity(user_id, hosted_event_id):
    """
        Function to check the authenticity of the user requesting to refund the payment
    """
    try:
        user_file_name = f"{user_id}.json"
        user_details = get_data_from_s3(user_file_name, USER_BUCKET_NAME)
        # check if the hosted_event_id is present in the user's profile
        authenticity = 1 if hosted_event_id in user_details['hosted_event_ids'] else -1
        print('authenticity',authenticity)
    except ClientError:
        LOGGER.error('wrong user id')
        authenticity = -1
        print('authenticity',authenticity)
    except KeyError:
        LOGGER.error('user_id not present, but requesting for admin dashboard')
        authenticity = -1
        print('authenticity',authenticity)
    return authenticity


def update_database(refund_status, booking_id, user_details, amount_to_be_refunded):
    """
        This Function to update the sql query
    """
    paid_amount_after_refund = int(user_details[0]['paid_amount']) - amount_to_be_refunded

    sql_query_command = f"UPDATE public.event_ticketing SET ticket_status = '{refund_status}', paid_amount = " \
        f"'{paid_amount_after_refund}' WHERE booking_id = '{booking_id}' "

    results = DB_CONNECTION.execute_sql_query(sql_query_command, function_name)
    return results


def get_data_from_s3(file_name, bucket_name):
    """
        Function to get the organiser details from S3
    """
    s3_response = client.get_object(Bucket=bucket_name, Key=file_name)
    response = json.loads(s3_response['Body'].read().decode())
    return response


def get_user_details(booking_id):
    """
        Function to get the user details from the database
    """
    command = "SELECT email_id, discount_code, designation, name, payment_id, organisation," \
              "ticket_type, time_stamp, ticket_status, booking_id, misc, event_name, " \
              "ticket_price, paid_amount FROM public.event_ticketing " \
              "WHERE booking_id = '" + booking_id + "' "

    rows = DB_CONNECTION.execute_sql_query(command, function_name)
    tickets = []
    for row in rows:
        ticket_entry = {
            "email_id": str(row[0]),
            "name": str(row[3]),
            "payment_id": str(row[4]),
            "organisation": str(row[5]),
            "ticket_type": str(row[6]),
            "time_stamp": str(row[7]),
            "ticket_status": str(row[8]),
            "booking_id": str(row[9]),
            "event_name": str(row[11]),
            "ticket_price": str(row[12]),
            "paid_amount": str(row[13]),
        }
        tickets.append(ticket_entry)
    return tickets


def check_amount_validity(refund_amount, paid_amount):
    """
        Checking the refund amount is greater than ticket price or equal to ticket price or less than ticket price
    """
    if refund_amount == paid_amount:
        ticket_status = "COMPLETE_REFUND"
    elif refund_amount < paid_amount:
        ticket_status = "PARTIAL_REFUND"
    else:
        LOGGER.info('Requested refund amount is greater than the amount paid')
        ticket_status = 0
    return ticket_status


def construct_razorpay_base_url(payment_id):
    """
        Function to construct the razorpay base URL
    """
    api_key = environ['api_key']
    api_secret = environ['api_secret']
    base_url = f"""https://{api_key}:{api_secret}@api.razorpay.com/v1/payments/{payment_id}"""
    print('base_url',base_url)
    return base_url


def refund_amount(amount_to_be_refunded, user_details):
    """
        Function to refund payment
    """
    paid_amount = int(user_details[0]['paid_amount'])
    payment_id = user_details[0]['payment_id']
    updated_ticket_status = check_amount_validity(amount_to_be_refunded, paid_amount)

    if updated_ticket_status:
        LOGGER.info(f'amount to be refunded {amount_to_be_refunded}')
        base_url = construct_razorpay_base_url(payment_id)
        capture_url = f"{base_url}/refund"
        payload = {
            "amount": int(amount_to_be_refunded) * 100
        }
        razorpay_response = requests.post(capture_url, data=payload)
        try:
            response = json.loads(razorpay_response.text)
            print('response',response)
            if response['entity'] == 'refund':
                return_statement = updated_ticket_status
                LOGGER.info('amount has been refund')
            else:
                return_statement = -1
                LOGGER.info('amount refund failed')
        except Exception as ex:
            LOGGER.error(ex)
            LOGGER.info('credential not matched')
            return_statement = -1
    else:
        LOGGER.info('refund amount was tampered')
        return_statement = -1
    return return_statement


def send_refund_mail(event):
    """
        Function to call the lambda to send refund email to the participant
    """
    client = boto3.client('lambda')
    client.invoke(
        FunctionName='event_send_invoice',
        InvocationType='RequestResponse',
        LogType='Tail',
        Payload=json.dumps(event)
    )


def lambda_handler(event, context):
    """
        Entry Function in Lambda
    """
    LOGGER.info(f'input to lambda: {event}')

    booking_id = event['body']["booking_id"]
    user_id = event['body']["user_id"]
    user_details = get_user_details(booking_id)
    hosted_event_id = user_details[0]['event_name']
    amount_to_be_refunded = int(event['body']['refund_amount'])

    organiser_file_name = f"hosted-events/{hosted_event_id}/organiser_details.json"
    organiser_details = get_data_from_s3(organiser_file_name, CONFERENCE_BUCKET_NAME)
    organiser_email_id = organiser_details['organiser_mail']

    user_authenticity = check_user_authenticity(user_id, hosted_event_id)
    if user_authenticity == 1:
        event.update(
            {
                "user_details": [
                    {
                        "name": user_details[0]["name"]
                    }
                ],
                "refund_mail_id": user_details[0]['email_id'],
                "status": "refund",
                "hosted_event_id": hosted_event_id,
                "organiser_email_id": organiser_email_id
            }
        )

        refund_status = refund_amount(amount_to_be_refunded, user_details)
        if refund_status != -1:
            update_database(refund_status, booking_id, user_details, amount_to_be_refunded)
            send_refund_mail(event)
    else:
        refund_status = -1
    return refund_status


if __name__ == "__main__":
    event = {"body":{
        "booking_id": "161c8eae",
        "refund_amount": 604,
        "event_name": "testkonfhub-mailtoall",
        "user_id": "1551250284"}
    }
    print(lambda_handler(event, " "))
