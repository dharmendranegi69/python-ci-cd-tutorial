"""
    Event class module file
"""
import base64
import json
import os

import boto3

client = boto3.client('s3')

BUCKET_NAME = os.environ['bucket_name']

class Event:
    """
        Event class
    """

    def __init__(self, hosted_event_id):
        self.hosted_event_id = hosted_event_id
        self.venue_details, self.event_date, self.event_name = self.get_event_details()
        self.tax_percentage, self.tax_inclusive, self.pg_charges_inclusive, self.pg_percentage = self.get_extra_charges()
        self.organiser_email_id, self.organiser_name, self.encoded_venue_details = self.get_organiser_details()

    def get_data_from_s3(self, file_name):
        """
            Helper function to get data from S3
        :param hosted_event_id: event id
        :param file_name: name of the file
        :return: file data from S3
        """
        folder_name = 'hosted-events/{}/{}.json'.format(self.hosted_event_id, file_name)
        response = client.get_object(Bucket=BUCKET_NAME, Key=folder_name)
        return json.loads(response['Body'].read().decode())

    def get_event_details(self):
        """
            Function to get the event_details from S3
        :return: venue, data and name of the event
        """
        event_details = self.get_data_from_s3('event_details')
        venue_details = event_details['event_venue']
        event_date = event_details['event_start_date']
        event_name = event_details['event_name']
        return venue_details, event_date, event_name

    def get_organiser_details(self):
        """
            Function to get the organiser details from S3
        :return:
        """
        organiser_details = self.get_data_from_s3('organiser_details')
        organiser_email_id = organiser_details['organiser_mail']
        organiser_name = organiser_details['organiser_name']
        venue_details_url = self.venue_details
        encoded_venue_details = (base64.b64encode(venue_details_url.encode('utf-8'))).decode()
        return organiser_email_id, organiser_name, encoded_venue_details

    def get_extra_charges(self):
        """
            Function to get the extra charges from S3
        :return: tax and PG related information
        """
        event_charges_data = self.get_data_from_s3('extra_charges')
        tax_percentage = event_charges_data['GST_or_Taxes']
        tax_inclusive = not (event_charges_data['taxes_charges_on_costumer'])
        pg_charges_inclusive = not (event_charges_data['payment_gateway_charges_on_costumer'])
        pg_percentage = event_charges_data['pg_charge']
        return tax_percentage, tax_inclusive, pg_charges_inclusive, pg_percentage