"""
    Function to convert json data to json stream and json list
"""
import json

def convert_to_json_stream(sorted_conference_data):
    """
        function to convert a JSON list to JSON stream (format to support S3 select)
    """
    convert_to_str = json.dumps(sorted_conference_data)
    str_formating = convert_to_str.replace('},','}')
    json_stream_formatted_data = str_formating.strip('[').strip(']')
    return json_stream_formatted_data

def convert_to_json_list(response_data):
    """
        fucntion to convert JSON stream to JSON list for further processing of the conferece data
    """
    data = response_data['Body'].read().decode()
    format_data = data.replace('}', '},', (data.count('}') - 1))
    str_formatted_data = '[' + format_data + ']'
    json_list_formatted_data = json.loads(str_formatted_data)
    return json_list_formatted_data