"""
    Participant class module
"""
class Participant:
    """
        Participant class
    """

    def __init__(self, event):
        self.name = event['name']
        self.email_id = event['email_id']
        self.organisation = event['organisation']
        self.designation = event['designation']
        self.booking_id = event['booking_id']
        self.phone_number = event['phone_number']
        self.paid_amount = event['paid_amount']
        self.payment_id = event['payment_id']
        self.ticket_type = event['ticket_type']
        self.ticket_price = event['ticket_price']
        if 'discount_code' in event.keys() and event['discount_code'] not in [None, '-']:
            self.discount_code = event['discount_code']
        else:
            self.discount_code = ''
        self.misc = event['misc'] if 'misc' in event.keys() else {}
