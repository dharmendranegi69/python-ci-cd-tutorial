"""
    S3 operations class is to perform read, write and s3 read from a file
"""
import json
import logging

import boto3

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


class S3Operations:

    def __init__(self, bucket_name):
        self.client = boto3.client('s3')
        self.s3_delete = boto3.resource("s3")
        self.bucket_name = bucket_name

    def get_data_from_s3(self, file_name):
        """
            Helper function to get data from S3 bucket
        :param file_name: Name of the file from which data is requested
        :return: data from S3 if the file was found else return 400 for any other error
        """
        try:
            response = self.client.get_object(Bucket=self.bucket_name, Key=file_name)
            data = json.loads(response['Body'].read().decode())
        except Exception as exception:
            data = 0
            LOGGER.error(exception)
        return data

    def put_data_to_s3(self, file_name, file_data):
        """
            Helper function to put data to S3
        :param file_name: Name of the file
        :param file_data: Data to be put into the file
        """
        self.client.put_object(Bucket=self.bucket_name,
                               Key=file_name,
                               Body=json.dumps(file_data))

    def upload_file_to_s3(self, local_file_name, file_name):
        """
            Helper function to upload files into s3
            :param local_file_name: Name of the locally generated file. Eg. details.csv
            :param file_name: File path where file has to be stored
        """
        self.s3_delete.Bucket(self.bucket_name)\
            .upload_file(local_file_name, file_name)

    def s3_select_query(self, file_name, expression):
        """
            Helper function to put s3 select query
        :param file_name: Name of the file
        :param file_data: Data to be put into the file
        """
        response = self.client.select_object_content(
            Bucket=self.bucket_name,
            Key=file_name,
            ExpressionType='SQL',
            Expression=expression,
            InputSerialization={
                'CompressionType': 'NONE',
                'JSON': {
                    'Type': 'Document',
                }
            },
            OutputSerialization={
                'JSON': {
                    'RecordDelimiter': '\n',
                }
            }
        )

        return response

    def s3_delete_query(self, file_name):
        """
          Function to delete s3 file
        :param file_name:
        :return: response
        """
        try:
            response = self.client.delete_object(Bucket=self.bucket_name, Key = file_name)
        except Exception as exception:
            response = 0
            LOGGER.error(exception)

        return response

    def download_file(self, file_name,file):
        """
        Function to download file from s3.
        :param file_name:
        :return:
        """
        try:
            response = self.client.download_file(Bucket=self.bucket_name, Key = file_name, Filename = file)
        except Exception as exception:
            response = 0
            LOGGER.error(exception)

        return response
