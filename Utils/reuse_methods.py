import json

from Utils.data_layer import DBConnection

DB_CONNECTION = DBConnection()


def read_json_file(filename):
    with open(filename) as f:
        data = json.load(f)

    return data


def input_validate_helper(event, mandatory_columns):
    missing_fields = list(set(mandatory_columns) - set(event.keys()))
    return missing_fields


def check_if_user_is_organiser(event_id, user_id):
    command = f"SELECT organiser_name FROM public.organisers WHERE event_id='{event_id}' AND user_id='{user_id}'"

    response = DB_CONNECTION.execute_sql_query(command, "verify if user is organiser")
    return len(response) > 0
