"""
    Data layer code to create a connection to PostgreSQL database and execute an SQL query
    Also sends an email to the dev team if the there was an error while executing the SQL command
"""
import json
import logging

import boto3
import psycopg2

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def get_config_values():
    """
        Function to get the config details like parameter_name, subject and
    :return:
    """
    global subject, lambda_function_name, parameter_name
    # subject and the lambda function if this code is running in production - Mumbai region
    lambda_region = boto3.session.Session().region_name
    if lambda_region != 'ap-south-1':
        subject = "Test Mode Failure"
        lambda_function_name = 'konfhub_generic_emailer'
        parameter_name = 'test_db_connection'
    else:
        subject = "Critical Error"
        lambda_function_name = 'generic_emailer'
        parameter_name = 'prod_db_connection'
    return subject, lambda_function_name, parameter_name


get_config_values()

client = boto3.client('ssm')
response = client.get_parameter(Name=parameter_name, WithDecryption=True)
# response = client.get_parameter(Name='prod_db_connection', WithDecryption=True)
credentials = json.loads(response['Parameter']['Value'])

DATABASE = credentials['DATABASE']
USER = credentials['USER']
PASSWORD = credentials['PASSWORD']
HOST = credentials['HOST']

class DBConnection:

    def __init__(self):
        self.CONNECTION = psycopg2.connect(database=DATABASE,
                                           user=USER,
                                           password=PASSWORD,
                                           host=HOST,
                                           port='5432'
                                           # sslmode="verify-ca",
                                           # sslrootcert="/opt/python/rds-combined-ca-bundle.pem"
                                           )

    def execute_sql_query(self, command, caller_lambda_function_name):
        """
            Function to execute an SQL query
        :param command: command to be executed
        :param caller_lambda_function_name: name of the lambda function that is requesting to execute SQL command. This
        will be used in sending an email to the dev team when a failure happens
        :return:
        """
        cur = self.CONNECTION.cursor()
        try:
            LOGGER.info(cur.execute(command))

            # return the details from database if the command is a select command
            if 'select' in command.lower():
                rows = ()
                for row in cur:
                    rows = rows + (row,)
                return_val = rows

            # commit the code if the command is for insert/update/delete
            elif 'insert' in command.lower() or 'update' in command.lower() or 'delete' in command.lower():
                self.CONNECTION.commit()
                return_val = 200
                if 'returning' in command.lower():
                    rows = ()
                    for row in cur:
                        rows = rows + (row,)
                    return_val = rows

        except Exception as error:
            # except error if a transaction is blocked because of some error in the previous query
            error_string = str(error)
            LOGGER.error(error_string)
            self.CONNECTION.rollback()
            self.send_email(caller_lambda_function_name, error_string)
            return_val = 400
        return return_val

    def send_email(self, caller_lambda_function_name, error_message):
        """
            Function to send an email when an error occurs
        """
        client = boto3.client('lambda')

        input_to_lambda = {
            "SENDER": "KonfHub <reachus@konfhub.com>",
            "RECIPIENT": "dev@konfhub.com",
            "SUBJECT": f"[{subject}] DB error in {caller_lambda_function_name} lambda",
            "BODY_TEXT": f"Hi Team, DB error occurred. Details: {error_message}. Please debug",
            "BODY_HTML": f"<html><head></head><body><p>Hi Team, DB error occurred in one of the lambdas of the functions. "
                         f"Details of the error: {error_message}. <br><br>Please debug.</p></body></html>"
        }
        client.invoke(FunctionName=lambda_function_name,
                      Payload=json.dumps(input_to_lambda),
                      InvocationType='Event',
                      LogType='Tail')
