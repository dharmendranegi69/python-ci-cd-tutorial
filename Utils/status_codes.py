""" Messages and codes based on Response """


def custom_response(status_code, message):
    return {
        "status_code": status_code,
        "message": message
    }


def delete_response():
    return {
        "status_code": 204
    }


def update_response(message="Update Successful"):
    return {
        "status_code": 200,
        "message": message
    }


def create_response(message="Create Successful"):
    return {
        "status_code": 201,
        "message": message
    }


def get_response(message="Get Successful"):
    return {
        "status_code": 200,
        "message": message,
    }


def bad_request(message="Bad Request"):
    return {
        "status_code": 400,
        "message": message
    }


def resource_not_found(message="Requested resource not found"):
    return {
        "status_code": 404,
        "message": message
    }


def access_denied_response(message="Access Denied, Forbidden"):
    return {
        "status_code": 403,
        "message": message
    }


def internal_server_error(message="Internal Server Error"):
    return {
        "status_code": 500,
        "message": message
    }
