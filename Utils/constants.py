CURRENCY_ID = {
    1: "INR",
    2: "USD"
}

ORGANISER_ROLE_ID = {
    "SUPER_ADMIN": 0,
    "EVENT_CREATOR": 1,
    "ADMIN": 2
}

EMAIL_CLIENTS = {
    "SES": 1,
    "SENDGRID": 2
}

EMAIL_TYPES = {
    "INDIVIDUAL":1,
    "BULK":2
}

FORM_TYPES = {
    1: "Text",
    2: "Date",
    3: "Emails",
    4: "Number",
    5: "PhoneNumber",
    6: "Url",
    7: "Dropdown",
    8: "Radio",
    9: "Consent",
    10: "Checkbox",

}
