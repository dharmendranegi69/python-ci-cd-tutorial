"""
    Python code to get the event details for all the hosted_event_ids of an organiser
"""
import json
import logging
import os
import threading
from datetime import datetime
import queue

import boto3
import botocore

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

host_id_event_details_dict = []
BUCKET_NAME = os.environ["conference_bucket_name"]

# increasing the number of S3 connection pools
client_config = botocore.config.Config(
    max_pool_connections=25,
)
client = boto3.client("s3", config=client_config)


def check_event_validity(event_end_date):
    """
        Function to check the date validity of the event
    :param event_end_date: the end date of the event
    :return: True if the event is a past event else returns False
    """

    end_date = datetime.strptime(event_end_date, '%Y-%m-%d')
    todays_date = datetime.now().date()

    is_past_event = bool(todays_date < end_date.date())
    return is_past_event


def get_event_details_json(hosted_event_id, event_details_queue):
    """
        Function to get the event details from S3 bucket
    :param hosted_event_id: hosted_event_id of the event
    :return: event details for the particular hosted_event_id
    """

    try:
        return_val = client.get_object(Bucket=BUCKET_NAME,
                                       Key=f"hosted-events/{hosted_event_id}/event_details.json")
        response = json.loads(return_val["Body"].read().decode())
        event_details_queue.put({
            "event_name": response["event_name"],
            "event_city": response["event_city"],
            "event_poster": response["event_poster"],
            "event_url": response["event_url"],
            "venue_details": response["event_venue"],
            "hosted_event_id": hosted_event_id,
            "event_status": check_event_validity(response["event_end_date"]),

            # role 1 means Admin role (hard coding it for now)
            "role": 1
        })
    except client.exceptions.NoSuchKey as error:
        LOGGER.error(f"the event id key '{hosted_event_id}' does not exist in the bucket")
        LOGGER.error(error)


def get_event_details_for_id_list(hosted_event_id_list):
    """
        Function to do multi-threading to get event details for all the hosted_event_ids
    :param hosted_event_id_list: list of hosted_event_ids hosted by the organiser
    :return: JSON containing list of hosted_event_ids and their respective details
    """
    all_event_details = []
    all_events_threads = []
    event_details_queue = queue.Queue()

    for id in hosted_event_id_list:
        thread = threading.Thread(target=get_event_details_json, args=(id, event_details_queue))
        thread.start()
        all_events_threads.append(thread)

    for thread in all_events_threads:
        thread.join()

    while True:
        if not event_details_queue.empty():
            all_event_details.append(event_details_queue.get())
        else:
            break
    return all_event_details
