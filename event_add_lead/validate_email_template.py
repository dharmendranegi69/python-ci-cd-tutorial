def send_lead_and_security_check_mail(entries):
    """ Entries contains all the variables passed through the method call """

    event_name = entries["event_name"]
    order_id = entries["order_id"]
    order_amount =  entries['order_amount']
    mail_text = {}
    mail_text["subject"] = f"{event_name} Attempted Registration and Security Check"
    mail_text["html_body"] = f"<p>Dear Team, <p></br><p>An attempt to book the ticket with " \
                        f"order id: {order_id} for order amount Rs. {order_amount} was made. " \
                        f"The event json is: {entries}</p>"""
    mail_text["text_body"] = f"Dear Team, An attempt to book the ticket with" \
                f"order id: {order_id} for order amount Rs. {order_amount} was made. " \
                f"The event json is: {entries}" \

    return mail_text
