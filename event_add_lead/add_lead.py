"""
    Lambda function to add the participant details to PostgreSQL database
    This is for the leads when the potential buyer presses the "Pay" button
    They become the leads for the follow-up for the organizers; also, when the transactions
    are stuck in the "Authorized" state, we can retrieve the user details and process
    them as a normal transaction and accept the payment
"""

import json
import logging
import re
import razorpay
import uuid
import os
import boto3
from Utils.constants import EMAIL_CLIENTS, EMAIL_TYPES
import validate_email_template  as template
from Utils.reuse_methods import input_validate_helper
from Utils.data_layer import DBConnection
from Utils.status_codes import (
    internal_server_error, bad_request)

api_key = os.environ['api_key']
api_secret = os.environ['api_secret']
stage = os.environ['STAGE']

DB_CONNECTION = DBConnection()
function_name = 'add_payment_registration_lead'

lambda_client = boto3.client('lambda')
client = razorpay.Client(auth=(api_key, api_secret))

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def create_individual_input(event, order_id):
    """
    Function to break down bulk booking user_details to single input
    :param event,order_id:
    :return: list of user details
    """
    user_details = event.pop('user_details')
    list_of_new_inputs = []

    for details in user_details:
        new_input = dict(event)

        new_input['name'] = details['name']
        new_input['phone_number'] = details['phone_number']
        new_input['organisation'] = details['organisation'] if 'organisation' in details.keys() else ''
        new_input['designation'] = details['designation'] if 'designation' in details.keys() else ''
        new_input['email_id'] = details['email_id'].lower().strip()
        new_input['country'] = details['country'] if 'country' in details.keys() else 'India'
        new_input['consent'] = details['consent'] if 'consent' in details.keys() else 'True'
        new_input['misc'] = details['form_details']
        new_input['booking_id'] = uuid.uuid4().hex[-8:]
        new_input['ticket_id'] = details['ticket_details']['ticket_id']
        new_input['ticket_name'] = details['ticket_details']['ticket_name']
        new_input['ticket_price'] = details['ticket_details']['price']
        new_input['payment_id'] = order_id
        new_input['ticket_status'] = 'LEAD'
        new_input['event_name'] = event['event_name']
        new_input['paid_amount'] = event['order_amount']
        new_input.pop('order_amount')

        list_of_new_inputs.append(new_input)

    return list_of_new_inputs


def validate_registration_lead_and_send_email(entries, order_id):
    """
      1.sending mail to konfhub when any lead is generated first time
      2.No duplicate lead will generate and send mail
    """
    email_template = template.send_lead_and_security_check_mail({
        "event_name": entries['event_name'],
        "order_id": order_id,
        "order_amount": entries['order_amount']
    })
    mail_text = {
        "client_type": EMAIL_CLIENTS["SES"],
        "email_type": EMAIL_TYPES["INDIVIDUAL"],
        "subject": email_template["subject"],
        "reply_to_address": "reachus@konfhub.com",
        "html_body": email_template["html_body"],
        "text_body": email_template["text_body"],
        "receipents": {
            "to": [{"name": 'konfhub', "email": f"reachus@konfhub.com"}],
        },
    }

    response = lambda_client.invoke(
        FunctionName='emailing_service',
        InvocationType='Event',
        LogType='Tail',
        Payload=json.dumps(mail_text)
    )
    print(response)


def get_event_details(data):
    """
    Function to get required event details
    :param valid_data:
    :return: update input with required event details
    """
    event_id = data['event_id']
    sql_query_command = f" SELECT name, tax_percentage, pg_percentage, tax_name, currency_name ,currency_id FROM public.events WHERE event_id = '{event_id}'"
    try:
        result = DB_CONNECTION.execute_sql_query(sql_query_command, 'validate_user')
        # update event details in main input
        data.update({
            "event_name": result[0][0],
            "tax_percentage": result[0][1],
            "pg_percentage": result[0][2],
            "tax_name": result[0][3],
            "currency_name": result[0][4],
            "currency_id": result[0][5]})
    except Exception as e:
        LOGGER.info(f'Query failed to get event details : {e}')
        return internal_server_error()
    return data


def check_user_in_db(data):
    """
    Function to validate user record exist.
    :param data:
    :return: payment_id if record exist else empty response
    """
    user_emails = ''
    ticket_ids = []
    return_value = []
    for each_input in data:
        event_id = each_input['event_id']
        user_emails += f"'{each_input['email_id'].lower().strip()}',"
        ticket_ids.append(str(each_input['ticket_id']))

    sql_query_command = f" SELECT ticket_id, email_id, ticket_number FROM public.event_ticketing WHERE  email_id in ({user_emails[:-1]}) " \
                        f"AND event_id = '{event_id}' and ticket_id in ({','.join(ticket_ids)}) and ticket_status='LEAD'"
    print('sql_query_command', sql_query_command)
    try:
        result = DB_CONNECTION.execute_sql_query(sql_query_command, 'validate_user')
        if len(result):
            for row in result:
                return_value.append({
                    "ticket_id": row[0],
                    "email_id": row[1],
                    "ticket_number": row[2],
                    "existing_lead": 1,
                })
    except Exception as e:
        LOGGER.info(f'Query failed to get existing lead details : {e}')
        return internal_server_error()

    return return_value


def create_order_id(event):
    """
    Function to create order id from razorpay.
    :param event:
    :return: order_id
    """
    data = {
        "amount": event['order_amount'],
        "currency": event['currency_name'],
        "receipt": event['order_receipt'],
        "payment_capture": '0',
        "notes": {
            "hosted_event_id": event['event_id']

        }
    }
    order_details = client.order.create(data)
    order_id = order_details['id']
    print('razorpay order details', order_details)

    return order_id


def sanitise_input(event):
    """
    Function to sanitise and validate the required user details
    :param event:
    :return: sanitised user data with valid fields
    """
    valid_fields = ["name", "organisation", "designation", "phone_number", "email_id", "consent", 'ticket_details',
                    'ticket_name', 'form_details', 'ticket_price', 'country']
    sanitised_user_data = {}
    for field in valid_fields:
        if field in event.keys():
            field_value = event[field]
            if field in ["name", "organisation", "designation"]:
                field_value = re.sub(r'[^a-zA-Z0-9_\s]+', '', field_value)
            sanitised_user_data[field] = field_value
    return sanitised_user_data


def validate_user_fields(valid_data):
    """
    Function to validate user mandatory fields and if it passes then sanitise the required details.
    :param valid_data:
    :return:
    """
    mandatory_fields = ["name", "phone_number", "email_id"]
    sanitised_data = []
    for each_input in valid_data['user_details']:
        input_validate_helper_response = input_validate_helper(each_input, mandatory_fields)
        # validate_user_details_response will be empty if all required fields are present.
        if input_validate_helper_response:
            return bad_request(message=f"Missing mandatory fields - {', '.join(input_validate_helper_response)}")
        else:
            # sanitising the required user fields when "validate_user_details_response" is empty
            result = sanitise_input(each_input)
            sanitised_data.append(result)

    return sanitised_data


def input_cleansing(event):
    """
    function to add only required field in input and remove unnecessary keys from input
    :param event:
    :return: valid input
    """
    valid_fields = ["order_amount", "event_id", "utm", "order_amount", "user_details",
                    "order_receipt", "coupon_id"]
    valid_input = {}
    for field in valid_fields:
        if field in event.keys():
            field_value = event[field]
            if field in ["utm"]:
                if len(event[field]) > 1:
                    for key, value in event[field].items():
                        result = value.replace('%20', ' ')  # replace '%20' from utm values if present
                        valid_input[key] = result
            valid_input[field] = field_value
    valid_input.pop(
        'utm')  # pop out utm from valid input because it will handled while generating input for insert query
    return valid_input


def insert_query(data):
    """
    Function to insert user details as lead
    :param data:
    :return:
    """

    payment_id = data['payment_id']
    event_name = data['event_name']
    event_id = data['event_id']
    utm_source = data['utm_source'] if 'utm_source' in data.keys() else '-'
    utm_medium = data['utm_medium'] if 'utm_medium' in data.keys() else '-'
    utm_campaign = data['utm_campaign'] if 'utm_campaign' in data.keys() else '-'
    tax_percentage = data['tax_percentage']
    tax_name = data['tax_name']
    pg_percentage = data['pg_percentage']
    currency_id = data['currency_id']
    name = data['name']
    phone_number = data['phone_number']
    organisation = data['organisation'] if 'organisation' in data.keys() else 'NA'
    designation = data['designation'] if 'designation' in data.keys() else 'NA'
    email_id = data['email_id']
    consent = data['consent']
    country = data['country']
    misc = data['misc']
    booking_id = data['booking_id']
    ticket_id = data['ticket_id']
    ticket_price = data['ticket_price']
    ticket_status = data['ticket_status']
    ticket_type = data['ticket_name']
    paid_amount = data['paid_amount']
    coupon_id = data['coupon_id'] if 'coupon_id' in data.keys() else '00000000-0000-0000-0000-000000000000'

    sql_command = f"INSERT INTO public.event_ticketing (payment_id, ticket_type, event_name, event_id, utm_source, utm_medium, utm_campaign, tax_percentage, tax_name, pg_percentage, currency_id, name, phone_number, organisation, designation, email_id, consent, misc, booking_id, ticket_id, ticket_price, ticket_status, paid_amount, coupon_id ,country) " \
                  f" values('{payment_id}', '{ticket_type}','{event_name}', '{event_id}', '{utm_source}', '{utm_medium}', '{utm_campaign}', {tax_percentage}, '{tax_name}', {pg_percentage}, {currency_id}, '{name}', '{phone_number}', '{organisation}', '{designation}', '{email_id}', {consent}, '{misc}', '{booking_id}', {ticket_id}, {ticket_price}, '{ticket_status}', {paid_amount},'{coupon_id}' , '{country}')"

    LOGGER.info('insert command: %s', sql_command)
    try:
        response = DB_CONNECTION.execute_sql_query(sql_command, 'insert lead')
    except Exception as e:
        LOGGER.exception(f'Query failed to insert lead details : {e}')
        return internal_server_error()
    return response


def insert_lead(order_id, ticket_numbers, create_individual_input_response, valid_user_data, check_user_response):
    """
    Function to insert or update leads record in db.
    :param order_id:
    :param ticket_numbers:
    :param create_individual_input_response:
    :param valid_user_data:
    :return: empty response if successfull , else status code with error message.
    """
    query_response = []
    if len(ticket_numbers):
        try:
            update_leads = f"UPDATE public.event_ticketing SET payment_id = '{order_id}' where ticket_number in ({','.join(str(number) for number in ticket_numbers)})"
            LOGGER.info('updated command: %s', update_leads)
            response = DB_CONNECTION.execute_sql_query(update_leads, function_name)
            LOGGER.info('updated successfully : %s', response)
        except Exception as e:
            LOGGER.exception(f'Query failed to update lead details : {e}')
            return {"status_code": 500, "message": "Internal Server Error"}

        # remove the common user details from input which are already present in db as lead based on email id and ticket id.
        for user_details in check_user_response:
            for index, each_input in reversed(list(enumerate(create_individual_input_response))):
                if (user_details['email_id'] == each_input['email_id'] and user_details['ticket_id'] == each_input[
                    'ticket_id']):
                    create_individual_input_response.pop(index)

        #  insert the remaining new ticket details in db
        for each_input in create_individual_input_response:
            try:
                insert_query_response = insert_query(each_input)
                # append insert_query_response  in query_response if status is not equal to 200
                query_response.append(insert_query_response) if insert_query_response != 200 else ''
            except Exception as e:
                LOGGER.exception(f'Query failed to insert lead details : {e}')
                return internal_server_error()
    else:
        # it will execute when there are no existing lead in db.
        # call insert_query function to insert new lead details in db.
        if stage == 'prod':
            validate_registration_lead_and_send_email(valid_user_data, order_id)
        for each_input in create_individual_input_response:
            try:
                insert_query_response = insert_query(each_input)
                # append insert_query_response  in query_response if status is not equal to 200
                query_response.append(insert_query_response) if insert_query_response != 200 else ''
            except Exception as e:
                LOGGER.exception(f'Query failed to insert new lead details : {e}')
                return internal_server_error()

    return query_response


def lambda_handler(event, context):
    """
        Entry point to the lambda function
    """
    LOGGER.info(f'input to lambda: {event}')
    lead_details = event['lead_details']
    lead_details.update({"event_id": event['event_id']})

    # call validate_user_fields function to validate required user filed and if pass sanitise them also else return error with missing values.
    user_fields = validate_user_fields(lead_details)

    # if "status_code" equal to 500 then validation will failed and return missing values.
    lead_details['user_details'] = user_fields
    if 'status_code' in user_fields:
        return user_fields

    # call validate input function to remove unwanted fields from root level and add only required fields in input
    valid_user_data = input_cleansing(lead_details)

    # get required event details from db
    event_details = get_event_details(valid_user_data)

    # if "status_code" equal to 500 return error.
    valid_user_data = event_details
    if 'status_code' in event_details:
        return event_details

    # call create_order_id function to create order_id in razorpay for given paid amount.
    order_id = create_order_id(valid_user_data)
    valid_user_data.pop('order_receipt')

    # create individual input to insert or update  lead details in db for each user details present in input.
    create_individual_input_response = create_individual_input(valid_user_data, order_id)

    # call validate user details in db function to check existing lead record for given input
    check_user_response = check_user_in_db(create_individual_input_response)
    if 'status_code' in check_user_response:
        return check_user_response

    # get ticket numbers from user details got from db
    ticket_numbers = list(map(lambda x: x["ticket_number"], check_user_response))
    # insert function to insert or update the lead recode in db.
    response = insert_lead(order_id, ticket_numbers, create_individual_input_response, valid_user_data,
                           check_user_response)

    # return order id if response does not have any error status, means it will be empty and length will be 0 then return error
    return_statement = {"status_code": 200, "order_id": order_id} if len(response) == 0 else internal_server_error()

    return return_statement


if __name__ == '__main__':
    event = {
        "event_id": "aa4b4d5e-1e44-45e9-5540-b002e87d95dd",
        "user_details": [
            {
                "email_id": "dharmendra@gmail.com",
                "name": "Parth",
                "organisation": "Codeops",
                "designation": "dadasdsa",
                "phone_number": "232323232",
                "consent": "True",
                "form_details": {},
                "ticket_details": {
                    "ticket_name": "Regular Ticket",
                    "ticket_id": 1,
                    "price": 1000
                }
            }
        ],
        "utm": {
            "utm_source": "AWS%20Workshop",
            "utm_medium": "Email",
            "utm_campaign": "AWS%20Workshop"
        },
        "order_amount": 100000,
        "order_receipt": "order_rcptid_01"
    }

    print(lambda_handler(event, ""))
