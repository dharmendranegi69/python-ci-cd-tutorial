"""
    Lambda to publish/un-publish and event
"""
import calendar
import json
import logging
import os

import boto3
from botocore.exceptions import ClientError

from Utils.data_layer import DBConnection
from Utils.s3operations import S3Operations

client_for_lambda = boto3.client('lambda')
client = boto3.client('s3')

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

DB_CONNECTION = DBConnection()
lambda_function_name = 'publish_event'

BUCKET_NAME = os.environ['BUCKET_NAME']
USERS_BUCKET_NAME = os.environ['USERS_BUCKET_NAME']
EVENT_BUCKET_NAME = os.environ['EVENT_BUCKET_NAME']

conference_s3_obj = S3Operations(EVENT_BUCKET_NAME)
s3_operation = S3Operations(BUCKET_NAME)


def check_user_authenticity(event):
    """
        Function to check the authenticity of the user requesting for event details
    """
    try:
        response = client.get_object(Bucket=USERS_BUCKET_NAME, Key=f"{event['user_id']}.json")
        user_data = json.loads(response['Body'].read().decode())
        # check if the hosted_event_id is present in the user's profile
        authenticity = 1 if event['hosted_event_id'] in user_data['hosted_event_ids'] else -1
    except ClientError:
        LOGGER.error('wrong user id')
        authenticity = -1
    except KeyError:
        LOGGER.error('user_id not present, but requesting for admin dashboard')
        authenticity = -1
    return authenticity


def delete_event_file(event):
    """
        Function to delete event file when event is unpublished.
    """

    file_name = event['event_url'].rsplit('/', 1)[-1]
    print('filename', file_name)
    response = s3_operation.s3_delete_query(file_name)
    print('delete file ', response)

    return response


def publish_event(event):
    """
        Function to make an event live by creating a file with the same name as event_url and
        the html_data is encoded in base64 and thus must be decoded
    """
    file_name = event['event_url'].rsplit('/', 1)[-1]
    hosted_event_id = event['hosted_event_id']

    map_event_url_query = f"INSERT INTO public.event_master (hosted_event_id, event_url)" \
                          f"values('{hosted_event_id}','{file_name}')" \
                          f"ON CONFLICT (hosted_event_id, event_url)" \
                          f"DO UPDATE SET event_url = '{file_name}' "

    result = DB_CONNECTION.execute_sql_query(map_event_url_query, lambda_function_name)

    # send mail to organiser only if transaction was successful
    if result == 200:
        create_event_page(event)
        send_email_to_organiser(event, 'publish')
        LOGGER.info("Event is live")
    return result

def update_event_url(event):
    """
    Function to update event url for given hosted event id
    :param event:
    :return:
    """
    new_url = event['new_url'].split('/')[-1]
    hosted_event_id = event['hosted_event_id']

    update_event_url_query = f"Update public.event_master SET event_url = '{new_url}' "\
                             f"where hosted_event_id = '{hosted_event_id}'"

    result = DB_CONNECTION.execute_sql_query(update_event_url_query, lambda_function_name)

    if result == 200:
        delete_event_file(event)
        create_event_page(event)
        send_email_to_organiser(event, 'publish')
        LOGGER.info("Event url update")
    return result


def unpublish_event(event):
    """
            Function to make an event unpublish.
    """
    hosted_event_id = event['hosted_event_id']

    unpublish_event_query = f"DELETE FROM public.event_master WHERE hosted_event_id = '{hosted_event_id}' "

    result = DB_CONNECTION.execute_sql_query(unpublish_event_query, lambda_function_name)

    # send mail to organiser only if transaction was successful
    if result == 200:
        send_email_to_organiser(event, 'unpublish')
        delete_event_file(event)

    return result


def create_event_page(event):
    """
    Function to create html file to redirect event url to actual directory
    :param event: input to the lambda function
    """
    hosted_event_id = event["hosted_event_id"]
    url = event['event_url'].rsplit('/', 1)[-1]
    event_name = event['event_name']
    event_city = event['event_city'].split(',')[0]
    year = event['event_start_date'].split('-')[0]
    month = calendar.month_name[int(event['event_start_date'].split('-')[1])]
    event_poster_url = event['event_poster'] if event['event_poster'] else "./images/logo.svg"


    extra_details_file_name = f"hosted-events/{hosted_event_id}/extra_details.json"
    event_details = conference_s3_obj.get_data_from_s3(extra_details_file_name)
    keywords = event_details["intrests"]

    content = f"""KonfHub - {event_name} - {event_city} | {event_name} - {event_city} in {event_city} | {event_name} - 
    {event_city} {year} | {event_name} - {event_city} in {month} | {keywords}"""

    html_data = f"""<!DOCTYPE html><html><head>
    <meta http-equiv="refresh" content="0; url=/event/#/{url}">
    <meta property="og:site_name" content="konfhub.com">
    <meta property="og:title" id="ogmetadatatitle" content="KonfHub - {event_name}">
    <meta property="og:description" id="ogmetadatadesc" content="">
    <meta property="og:image" id="ogImg" content="{event_poster_url}">
    <meta property="og:url" id="ogUrl" content="/event/#/{url}">
    <meta property="og:type" content="website"><meta name="keywords" id="metakeywords" content="{content}">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119749580-1"></script>
    <script>window.dataLayer = window.dataLayer || [];function gtag() {{dataLayer.push(arguments);}}
        gtag('js', new Date());
        gtag('config', 'UA-119749580-1');
    </script>
    </head><body></body></html>"""

    response = client.put_object(Bucket=BUCKET_NAME,
                                 Key=url,
                                 Body=html_data.encode('utf-8', 'xmlcharrefreplace'),
                                 ContentType='text/html')

    LOGGER.info('file created. Event is now live')

    return response


def send_email(event_url, event_name, organiser_mail_id, status):
    """
        Function to send mail to the organizer
    """
    if status == 'publish':
        input_to_lambda = {
            "SUBJECT": f"{event_name} is Published!",
            "SENDER": f"KonfHub <reachus@konfhub.com>",
            "RECIPIENT": organiser_mail_id,
            "BCC": f"KonfHub <reachus@konfhub.com>",
            "BODY_TEXT": f"Dear Organiser,/n/nThis is to inform you that {event_name} event is now live."
                         f" Please note that will take a few minutes for the updated content to reflect "
                         f"in the event's page: www.konfhub.com/{event_url} \n\n"
                         f"From KonfHub, we are here to help you make your event a big success. "
                         f"Please don't hesitate to contact us at reachus@konfhub."
                         f"com (just reply to this email id) or "
                         f"call us at +91 96321 77909 (Hari Kiran, Co-Founder, KonfHub) for any help, "
                         f"suggestions or queries."
                         f"\nRegards,\n\nKonfHub Team",
            "BODY_HTML": f"<p>Dear Organiser,</p><p>This is to inform you that <b>{event_name}</b> event "
                         f"is now live. Please note that it will take a few "
                         f"minutes for the updated content to "
                         f"reflect in the event's page: www.konfhub.com/{event_url} "
                         f"<p>From KonfHub, we are here to help you make your event "
                         f"a big success. Please don't hesitate "
                         f"to contact us at reachus@konfhub.com (just reply to this email id) or "
                         f"call us at +91 96321 77909 (Hari Kiran, Co-Founder, KonfHub) for any help, "
                         f"suggestions or queries.</p><p>Regards,<br/>KonfHub Team</p>"
        }
    else:
        input_to_lambda = {
            "SUBJECT": f"{event_name} is Unpublished!",
            "SENDER": f"KonfHub <reachus@konfhub.com>",
            "RECIPIENT": organiser_mail_id,
            "BCC": f"KonfHub <reachus@konfhub.com>",
            "BODY_TEXT": f"Dear Organiser,/n/nThis is to inform you that {event_name} event is now unpublished."
                         f" Please note that will take a few minutes for the updated content to reflect "
                         f"in the event's page: www.konfhub.com/{event_url} \n\n"
                         f"From KonfHub, we are here to help you make your event a big success. "
                         f"Please don't hesitate to contact us at reachus@konfhub."
                         f"com (just reply to this email id) or "
                         f"call us at +91 96321 77909 (Hari Kiran, Co-Founder, KonfHub) for any help, "
                         f"suggestions or queries."
                         f"\nRegards,\n\nKonfHub Team",
            "BODY_HTML": f"<p>Dear Organiser,</p><p>This is to inform you that <b>{event_name}</b> event "
                         f"is now unpublished."
                         f"Please don't hesitate "
                         f"to contact us at reachus@konfhub.com (just reply to this email id) or "
                         f"call us at +91 96321 77909 (Hari Kiran, Co-Founder, KonfHub) for any help, "
                         f"suggestions or queries.</p><p>Regards,<br/>KonfHub Team</p>"
        }

    client_for_lambda.invoke(
        FunctionName='generic_emailer',
        InvocationType='Event',
        LogType='Tail',
        Payload=json.dumps(input_to_lambda)
    )


def send_email_to_organiser(event, status):
    """
       checking that hosted event id of JetBrains and testing konfhub then we are not sending mail.
    """
    hosted_event_id = event["hosted_event_id"]
    organiser_file_name = f"hosted-events/{hosted_event_id}/organiser_details.json"
    event_url = event['event_url'].rsplit('/', 1)[-1]
    organiser_mail = conference_s3_obj.get_data_from_s3(organiser_file_name)
    # sending mail to reachus mail id if event is not test event
    if 'testkonfhub' not in event['hosted_event_id'].lower():
        # sending mail to the konfhub and organiser when event make live
        send_email(event_url, event['event_name'], organiser_mail['organiser_mail'], status)


def lambda_handler(event, context):
    """
        Entry function in lambda
    """
    LOGGER.info(f"input to lambda: {event}")
    if 'body' in event.keys():
        event.update(event.pop("body"))
    hosted_event_id = event["hosted_event_id"]

    user_authenticity = check_user_authenticity(event)
    if user_authenticity == 1:
        event_details_file_name = f"hosted-events/{hosted_event_id}/event_details.json"
        event_details = conference_s3_obj.get_data_from_s3(event_details_file_name)
        if event_details:
            event.update(event_details)
            if 'new_url' in event.keys():
                result = update_event_url(event)
                return_status = event['new_url'] if result == 200 else 400
            elif event['publish']:
                result = publish_event(event)
                return_status = event['event_url'] if result == 200 else 400
            else:
                return_status = unpublish_event(event)
        else:
            return_status = 404
            LOGGER.info("Event Details Not Found For Event Id")
    # when user is not authorized so they cannot proceed to make live the URL
    else:
        LOGGER.info("User not authorized to publish/un-publish event")
        return_status = 403
    return return_status


if __name__ == '__main__':
    input_details = {
        "hosted_event_id": "testkonfhube5f9bb0f",
        "publish": 1,
        "user_id": '1551250284'
    }
    os.environ['BUCKET_NAME'] = "konfhub.com"
    os.environ['EVENT_BUCKET_NAME'] = "konfhub-conference-details"
    os.environ['USERS_BUCKET_NAME'] = "konfhub-user-details"
    print(lambda_handler(input_details, ''))
